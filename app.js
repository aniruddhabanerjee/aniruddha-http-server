const path = require('path')
const express = require('express')
const uuid = require('uuid')
const dotenv = require('dotenv')

dotenv.config()

const app = express()

const port = process.env.PORT

app.get('/html', (req, res) => {
  res
    .status(200)
    .sendFile(path.join(__dirname, 'public', 'index.html'), (error) => {
      if (error) {
        console.log(error)

        res.setHeader('content-type', 'application/json')
        res
          .status(404)
          .json({
            message: 'File Not Found',
          })
          .end()
      } else {
        res.end()
      }
    })
})

app.get('/json', (req, res) => {
  res
    .status(200)
    .sendFile(path.join(__dirname, 'public', 'slide.json'), (error) => {
      if (error) {
        console.log(error)

        res.setHeader('content-type', 'application/json')
        res
          .status(404)
          .json({
            message: 'File Not Found',
          })
          .end()
      } else {
        res.end()
      }
    })
})

app.get('/uuid', (req, res) => {
  res
    .setHeader('content-type', 'application/json')
    .status(200)
    .send({
      uuid: uuid.v4(),
    })
    .end()
})

app.get('/status/:status_code', (req, res) => {
  const statusCode = Number(req.params.status_code)
  try {
    res.setHeader('content-type', 'application/json')

    if (statusCode >= 100 && statusCode < 600) {
      res
        .status(statusCode)
        .json({
          message: `Return a response with ${statusCode} status code`,
        })
        .end()
    } else {
      res.setHeader('content-type', 'application/json')
      res
        .status(400)
        .json({
          error: 'Invalid status code',
        })
        .end()
    }
  } catch (error) {
    console.error(error)

    res.setHeader('content-type', 'application/json')
    res
      .status(400)
      .json({
        error: 'Invalid status code',
      })
      .end()
  }
})

app.get('/delay/:delay_in_second', (req, res) => {
  const delay = Number(req.params.delay_in_second)
  if (Number.isInteger(delay) && delay < 0) {
    res
      .status(400)
      .json({
        error: 'Enter a Positive Number',
      })
      .end()
  } else if (Number.isInteger(delay)) {
    setTimeout(function () {
      res
        .status(200)
        .json({
          message: 'Done',
        })
        .end()
    }, delay * 1000)
  } else {
    res
      .status(400)
      .json({
        error: 'Enter a Valid Number',
      })
      .end()
  }
})

app.use((req, res) => {
  res.status(500)
  res
    .json({
      error: {
        message: 'Page Not Found',
      },
    })
    .end()
})

app.listen(port, () => {
  console.log(`Server is running at ${port}...`)
})
